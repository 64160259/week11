package com.panisa.week11;

public class Dog extends Animal{

    public Dog(String name) {
        super(name, 4);
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }

    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }
    
}
