package com.panisa.week11;

public class Human extends Animal{

    public Human(String name) {
        super(name, 2);
    }    
    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }

    @Override
    public String toString() {
        return "Human(" + this.getName() + ")";
    }
    
}
