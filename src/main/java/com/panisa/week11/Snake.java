package com.panisa.week11;

public class Snake extends Animal implements Crawable {
    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }

    @Override
    public void craw() {
        System.out.println(this + " craw.");
    }

    @Override
    public String toString() {
        return "Snake(" + this.getName() + ")";
    }
}
