package com.panisa.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        Bird bird1 = new Bird("Jidrid");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.landing();
        bird1.takeoff();
        bird1.walk();
        Plane plane1 = new Plane("Boeing", "Boeing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();
        Submarine sub = new Submarine("Prayuth", "PraYuth Engine");
        sub.swim();
        Snake snake1 = new Snake("BamBam");
        snake1.eat();
        snake1.sleep();
        snake1.craw();
        Crocodile croc = new Crocodile("Chala-one");
        croc.eat();
        croc.sleep();
        croc.craw();
        croc.swim();
        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        Dog dog1 = new Dog("Max");
        dog1.eat();
        dog1.sleep();
        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();
        Human human1 = new Human("Preem");
        human1.eat();
        human1.sleep();

        Flyable[] flyableObjects = {bat1, bird1, plane1};
        for(int i=0; i<flyableObjects.length;i++) {
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        }

        Swimable[] swimableObjects = {fish1, croc};
        for(int i=0; i<swimableObjects.length;i++) {
            swimableObjects[i].swim();
        }

        Crawable[] crawableObjects = {snake1, croc};
        for(int i=0; i<crawableObjects.length;i++) {
            crawableObjects[i].craw();
        }

        Walkable[] walkableObjects = {bird1, rat1};
        for(int i=0; i<walkableObjects.length;i++) {
            walkableObjects[i].walk();
        }
    }
}
