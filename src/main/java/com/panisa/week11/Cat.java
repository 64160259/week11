package com.panisa.week11;

public class Cat extends Animal {

    public Cat(String name) {
        super(name, 4);
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }
    
    @Override
    public String toString() {
        return "Cat(" + this.getName() + ")";
    }
}
